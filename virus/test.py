import unittest

from . import insert


class testInsert(unittest.TestCase):
    def setUp(self):
        self.arr = [1, 2, 3, 4]

    def test_1(self):
        self.assertEqual(insert([], 1), [1])

    def test_2_f(self):
        self.assertEqual(insert([1], 0), [0, 1])

    def test_2_l(self):
        self.assertEqual(insert([0], 1), [0, 1])

    def test_last(self):
        self.assertEqual(insert(self.arr, 5), [1, 2, 3, 4, 5])

    def test_same(self):
        self.assertEqual(insert(self.arr, 3), [1, 2, 3, 3, 4])

    def test_between(self):
        self.assertEqual(insert(self.arr, 2.5), [1, 2, 2.5, 3, 4])

    def test_first(self):
        self.assertEqual(insert(self.arr, 0), [0, 1, 2, 3, 4])


if __name__ == '__main__':
    unittest.main()
